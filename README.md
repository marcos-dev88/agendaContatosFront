# Agenda de Contatos Web (Front-End)

Esta é a interface do projeto 'Agenda de Contatos' criada para vir a facilitar a forma a qual você salva os seus contatos em uma plataforma web computador.

### Instalação:

1. Instale o Xampp em seu computador, você pode obter o mesmo neste link: https://www.apachefriends.org/download.html (Instale a versão que é compatível com o seu sistema operacional; 
2. Faça um clone deste projeto com este código:  git clone https://gitlab.com/Marcos_Piesske_Dev/agendaContatosFront
3. Mova então o projeto clonado para dentro da pasta 'htdocs' que se encontra onde você instalou o Xampp;
4. Após colocar o projeto na pasta 'htdocs', volte para a pasta principal do Xampp.
5. Execute este arquivo: 

#### Screenshot:
![star_xampp](https://gitlab.com/Marcos_Piesske_Dev/agendaContatosFront/-/blob/master/start_xampp.png) 

### Testar aplicação:
Basta ir em seu navegador e acessar: localhost/nomeDoProjetoClonado

### OBS:
Apenas a interface está funcionando. Para que ela funcione 100%, deverás configurar o Back-End. 

## Instalar o Back-End da aplicação:

1. Acessar o link: https://gitlab.com/Marcos_Piesske_Dev/agendaContatoSpring
2. Seguir o passo a passo no README.md do repositório acima.

